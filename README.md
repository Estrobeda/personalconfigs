# PersonalConfig
Personal configurations and setups for Estrobeda's personal workflow.


## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Installation
TBD - Currently not needed as there's no setup scripts.

## Usage
TBD - Currently not needed as there's no setup scripts.


## Contributing
Since this is a personal configuration & setup project, contributions will generally not be accepted. However, feel free to for the project and give suggestions to an improved workflow.
Contributions that I will likely accept however are setup/configuration fixes if found.

Feel free to fork this project to make your own workflow.

## Authors and acknowledgment
- Estrobeda (Maintainer)

## License
Unlicensed, public domain
